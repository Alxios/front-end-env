import Vue from 'vue'
import Hello from './component/Hello'

new Vue({
  el: 'body',
  components: { Hello }
})
